import  {forEach,forEachObject,unless,times,every,some,sortBy} from "./LIB/es6-functional.js";




function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}

function fn()
{
    document.getElementById("TestCase").innerHTML = "This content changed from script";






    var people = [
        {firstname: "aaFirstName", lastname: "cclastName"},
        {firstname: "ccFirstName", lastname: "aalastName"},
        {firstname:"bbFirstName", lastname:"bblastName"}
    ];

    console.log(people);

    //sorting with respect to firstname
    console.log("FirstName sort manually",people.sort((a,b) => {

        return (a.firstname < b.firstname) ? -1 : (a.firstname > b.firstname) ? 1 : 0;


    }));

    console.log("#######################################################");








}
/*



var apple = new function() {
    this.type = "macintosh";
    this.color = "red";
    this.getInfo = function () {
        return this.color + ' ' + this.type + ' apple';
    };
}




apple.color = "reddish";
alert(apple.getInfo());




 */

if(document.readyState != "loading")
{
    fn();


}
else
{
    document.addEventListener("DOMContentLoaded", fn);
}



 


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhY2thZ2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFhX3NhbXBsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAge2ZvckVhY2gsZm9yRWFjaE9iamVjdCx1bmxlc3MsdGltZXMsZXZlcnksc29tZSxzb3J0Qnl9IGZyb20gXCIuL0xJQi9lczYtZnVuY3Rpb25hbC5qc1wiO1xyXG5cclxuXHJcblxyXG5cclxuZnVuY3Rpb24gc2xlZXAoZGVsYXkpIHtcclxuICAgIHZhciBzdGFydCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xyXG4gICAgd2hpbGUgKG5ldyBEYXRlKCkuZ2V0VGltZSgpIDwgc3RhcnQgKyBkZWxheSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGZuKClcclxue1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJUZXN0Q2FzZVwiKS5pbm5lckhUTUwgPSBcIlRoaXMgY29udGVudCBjaGFuZ2VkIGZyb20gc2NyaXB0XCI7XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbiAgICB2YXIgcGVvcGxlID0gW1xyXG4gICAgICAgIHtmaXJzdG5hbWU6IFwiYWFGaXJzdE5hbWVcIiwgbGFzdG5hbWU6IFwiY2NsYXN0TmFtZVwifSxcclxuICAgICAgICB7Zmlyc3RuYW1lOiBcImNjRmlyc3ROYW1lXCIsIGxhc3RuYW1lOiBcImFhbGFzdE5hbWVcIn0sXHJcbiAgICAgICAge2ZpcnN0bmFtZTpcImJiRmlyc3ROYW1lXCIsIGxhc3RuYW1lOlwiYmJsYXN0TmFtZVwifVxyXG4gICAgXTtcclxuXHJcbiAgICBjb25zb2xlLmxvZyhwZW9wbGUpO1xyXG5cclxuICAgIC8vc29ydGluZyB3aXRoIHJlc3BlY3QgdG8gZmlyc3RuYW1lXHJcbiAgICBjb25zb2xlLmxvZyhcIkZpcnN0TmFtZSBzb3J0IG1hbnVhbGx5XCIscGVvcGxlLnNvcnQoKGEsYikgPT4ge1xyXG5cclxuICAgICAgICByZXR1cm4gKGEuZmlyc3RuYW1lIDwgYi5maXJzdG5hbWUpID8gLTEgOiAoYS5maXJzdG5hbWUgPiBiLmZpcnN0bmFtZSkgPyAxIDogMDtcclxuXHJcblxyXG4gICAgfSkpO1xyXG5cclxuICAgIGNvbnNvbGUubG9nKFwiIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1wiKTtcclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG59XHJcbi8qXHJcblxyXG5cclxuXHJcbnZhciBhcHBsZSA9IG5ldyBmdW5jdGlvbigpIHtcclxuICAgIHRoaXMudHlwZSA9IFwibWFjaW50b3NoXCI7XHJcbiAgICB0aGlzLmNvbG9yID0gXCJyZWRcIjtcclxuICAgIHRoaXMuZ2V0SW5mbyA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb2xvciArICcgJyArIHRoaXMudHlwZSArICcgYXBwbGUnO1xyXG4gICAgfTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuYXBwbGUuY29sb3IgPSBcInJlZGRpc2hcIjtcclxuYWxlcnQoYXBwbGUuZ2V0SW5mbygpKTtcclxuXHJcblxyXG5cclxuXHJcbiAqL1xyXG5cclxuaWYoZG9jdW1lbnQucmVhZHlTdGF0ZSAhPSBcImxvYWRpbmdcIilcclxue1xyXG4gICAgZm4oKTtcclxuXHJcblxyXG59XHJcbmVsc2Vcclxue1xyXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZm4pO1xyXG59XHJcblxyXG5cclxuXHJcbiBcclxuXHJcbiJdfQ==
