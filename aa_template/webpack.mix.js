const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');




mix.browserSync({

    proxy:
        {
             target: "localhost/aa_sample", // can be [virtual host, sub-directory, localhost with port]
            //    ws: true // enables websockets
            // target: "localhost/register"



        },

    files: [
       'zz_trigger/**/*',


    ]


});
